# fzf added keybinds, maining ctrl - R for lookup of commands

# [[ -f ~/.Xmodmap ]] && xmodmap ~/.Xmodmap
# Consider using zsh and vi mode soon
# zsh
# set -o vi
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
source /usr/share/fzf/completion.bash
source /usr/share/fzf/key-bindings.bash
source ~/.git-completion.bash
source ~/.aliases
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"
export PATH="$PATH:$HOME/.node_modules_global/bin"
export PATH="$PATH:$HOME/.gem/ruby/2.5.0/bin"
http_proxy="http://127.0.0.1:3128"
https_proxy="http://127.0.0.1:3128"
ftp_proxy="ftp://127.0.0.1:3128"
HTTP_PROXY="http://127.0.0.1:3128"
HTTPS_PROXY="http://127.0.0.1:3128"
FTP_PROXY="ftp://127.0.0.1:3128"
