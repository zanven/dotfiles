#!/bin/sh
# cp thisfile /etc/NetworkManager/dispatcher.d/setnoproxy.sh
# chmod +x /etc/NetworkManager/dispatcher.d/setnoproxy.sh

NoProxy="NoProxy     localhost, *.local, 127.0.0.*, 10.*, 192.168.*, *.corp.telstra.com, *.in.telstra.com.au, *.team.telstra.com, *.wg.dir.telstra.com, *.unily.com, *.v.nsa.nexus.telstra.com.au #, *.telstra.com.au, *.telstra.com"
case "$2" in
    down|vpn-down|up)
        IP=( $(ip addr show | grep 'inet ' | awk '{ print $2 }') )
        if [[ "${IP[@]}" =~ ( |^)10\..* ]]; then
            echo "Work net"
        else
            echo "local net"
            NoProxy="NoProxy    *"
        fi
        sudo sed "s/^NoProxy.*$/$NoProxy/g" -i /etc/cntlm.conf
        echo "$NoProxy"
        sudo systemctl restart cntlm
        ;;
    custom-up)
        sudo sed "s/^NoProxy.*$/$NoProxy/g" -i /etc/cntlm.conf
        sudo systemctl restart cntlm
esac
